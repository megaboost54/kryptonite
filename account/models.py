from django.db import models

class User(models.Model):
    user_id                         = models.AutoField(primary_key=True)
    user_name                       = models.CharField(max_length=200)
    user_email                      = models.EmailField(max_length=200)
    user_password                   = models.CharField(max_length=50)
    profile_image_url               = models.CharField(max_length=2085)

    def __str__(self):
        return self.user_name