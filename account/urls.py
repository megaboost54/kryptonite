from django.urls import path
from . import views
from django.contrib.auth.views import LogoutView
from django.urls import path, re_path

urlpatterns = [
    path('', views.index),
    path('login', views.login, name='login'),
    path('signup', views.signup, name='signup'),
    path('logout', LogoutView.as_view(), name='logout')
]

