from django import forms
from reminder_models import *

class ReminderCreateForm(forms.ModelForm):
    class Meta:
        model = Reminder
        fields = [
            
            'med_name',                        
            'med_form',                        
            'med_strength_units',              
            'med_strength',                    
            'med_condition',                  
            'med_schedule',                    
            'med_dosage',                      
            'med_time_of_day',                 
            'med_time',                       
            'med_duration_start_date',         
            'med_duration_end_date',           
            'med_refill_current_pills',        
            'med_refill_alert_when_pills',    
            'med_refill_alert_time',           
            'med_Rx_number',                   
            'med_instructions_food',          
            'med_instructions_note',

        ]
