from django.db import models
from django.forms import ModelForm
from account.models import User 


MED_FORM_CHOICES = [
    ('PILL', 'Pill'),
    ('SOLUTION', 'Solution'),
    ('INJECTION', 'Injection'),
    ('POWDER', 'Powder'),
    ('DROPS', 'Drops'),
    ('INHALER', 'Inhaler'),
    ('OTHER', 'Other')
]


DOSAGE_STRENGTH_CHOICES = [
    ('GM', 'gm'),
    ('IU', 'IU'),
    ('MCG', 'mcg'),
    ('ML', 'ml'),
    ('TB', 'tb'),
    ('TSP', 'tsp'),
    ('OZ', 'oz')
]


PRESCRIBED_SCHEDULE_CHOICES = [
    ('YES', 'Yes'),
    ('NO', 'No'),
    ('ONLY AS NEEDED', 'Only as needed')
]


DOSAGE_FREQ_CHOICES = [
    ('ONCE DAILY', 'Once daily'),
    ('TWICE DAILY', 'Twice daily'),
    ('3 TIMES A DAY', '3 times a day'),
    ('4 TIMES A DAY', '4 times a day'),
    ('6 TIMES A DAY', '6 times a day'),
    ('EVERY 6 HOURS', 'Every 6 hours'),
    ('ONLY AS NEEDED', 'Only as needed'),
    ('OTHER', 'Other')
]


DOSAGE_TOD_CHOICES = [
    ('MORNING', 'Morning'),
    ('NOON', 'Noon'),
    ('EVENING', 'Evening'),
    ('NIGHT', 'Night')
]


DOSAGE_HOW_OFTEN_CHOICES = [
    ('SPECIFIC TIMES OF THE DAY', 'Specific times of the day'),
    ('EVERY NUMBER OF HOURS', 'Every number of hours'),
    ('OTHER', 'other')
]

DOSAGE_HRS_INTERVAL_CHOICES = [
    ('1','1'),
    ('2','2'),
    ('3','3'),
    ('4','4'),
    ('6','6'),
    ('8','8'),
    ('12','12')
]

DOSAGE_INSTR_CHOICES = [
    ('BEFORE FOOD', 'Before food'),
    ('AFTER FOOD', 'After food'),
    ('WITH FOOD', 'With food')
]


# class Saved_med(models.Model):
#     reminder_id =  models.ManyToManyField(Reminder)

class Reminder(models.Model):
    user_id                                 = models.ManyToManyField(User)
    med_id                                  = models.AutoField(primary_key=True)
    med_name                                = models.CharField(max_length=250)
    med_form                                = models.CharField(max_length=9, choices=MED_FORM_CHOICES)
    med_strength_units                      = models.CharField(max_length=3, choices=DOSAGE_STRENGTH_CHOICES)
    med_strength                            = models.PositiveIntegerField(default=0)
    med_condition                           = models.TextField(max_length=200)
    med_prescribed_schedule                 = models.CharField(max_length=14, choices=PRESCRIBED_SCHEDULE_CHOICES)
    med_dosage_freq                         = models.CharField(max_length=14, choices=DOSAGE_FREQ_CHOICES)
    med_dosage_time_of_day                  = models.CharField(max_length=21, choices=DOSAGE_TOD_CHOICES)
    med_dosage_how_often                    = models.CharField(max_length=25, choices=DOSAGE_HOW_OFTEN_CHOICES)
    med_dosage_hrs_interval                 = models.IntegerField(default=0, choices=DOSAGE_HRS_INTERVAL_CHOICES)
    med_dosage_specific_num_of_times_a_day  = models.PositiveIntegerField(default=0)
    med_rem_time                            = models.TimeField(null=True, blank=True)
    med_duration_start_date                 = models.DateField(null=True, blank=True)
    med_duration_end_date                   = models.DateField(null=True, blank=True)
    med_refill_current_pills                = models.PositiveIntegerField(default=0)
    med_refill_alert_when_pills             = models.PositiveIntegerField(default=0)
    med_refill_alert_time                   = models.TimeField(null=True, blank=True)
    med_Rx_number                           = models.PositiveIntegerField(default=0)
    med_instructions_food                   = models.CharField(max_length=11, choices=DOSAGE_INSTR_CHOICES)
    med_instructions_note                   = models.CharField(max_length=200)
    med_form_icon_url                       = models.CharField(max_length=2085)

    def __str__(self):
        return self.med_name


class ReminderForm(ModelForm):
    class Meta:
        model = Reminder
        fields = ['med_name', 'med_form', 'med_strength_units', 'med_strength', 'med_rem_time', 
                  'med_duration_start_date', 'med_duration_end_date', 'med_instructions_food', 'med_instructions_note']


class RefillForm(ModelForm):
    class Meta:
        model = Reminder
        fields = ['med_refill_current_pills', 'med_refill_alert_when_pills', 'med_refill_alert_time', 'med_refill_alert_time']